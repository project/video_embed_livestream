<?php

namespace Drupal\video_embed_livestream\Plugin\video_embed_field\Provider;

use Drupal\Component\Utility\Html;
use Drupal\video_embed_field\ProviderPluginBase;

/**
 * A Livestream provider plugin for video embed field.
 *
 * @VideoEmbedProvider(
 *   id = "livestream",
 *   title = @Translation("Livestream")
 * )
 */
class Livestream extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $iframe = [
      '#type' => 'video_embed_iframe',
      '#provider' => 'livestream',
      '#url' => sprintf('https://livestream.com/%s/player', $this->getVideoId()),
      '#query' => [],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
    return $iframe;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return $this->oEmbedData()->thumbnail_url;
  }

  /**
   * Get the Livestream oembed data.
   *
   * @return array
   *   An array of data from the oembed endpoint.
   */
  protected function oEmbedData() {
    return json_decode(file_get_contents('https://livestream.com/oembed?url=' . $this->getInput()));
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    $matches = array();
    preg_match('/livestream.com\/(.*)/', $input, $matches);
    if ($matches && !empty($matches[1])) {
      // Security check, this is the only place where we use user input.
      $match = explode('?', $matches[1]);
      return Html::escape($match[0]);
    }
    // Otherwise return FALSE.
    return FALSE;

  }
}
