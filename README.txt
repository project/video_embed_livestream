An integration for Livestream videos into Video Embed Field. Videos hosted at livestream.com can be added into the video embed field using the video link.

Just add the video URL to the field, such as: https://livestream.com/livestream/3camkit/videos/150060311
